package ar.com.seguridadalerta.sharedPref;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ar.com.seguridadalerta.api.GsonHelper;
import ar.com.seguridadalerta.api.model.QrGpsData;

public class PersistenceHelper {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public PersistenceHelper(Context context){
        sharedPreferences = context.getSharedPreferences("QR_TRACK", Context.MODE_PRIVATE);
    }

    public void saveData(String key, String value){
        Log.d("map values", "Saving Data");
        editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
        editor.commit();
    }

    public void removeData(String key){
        Log.d("map values", "Delete Data");
        editor = sharedPreferences.edit();
        editor.remove(key) ;
        editor.apply();
        editor.commit();
    }

    public void removeAllData(){
        editor = sharedPreferences.edit();
        Map<String, ?> allEntries = sharedPreferences.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            editor.remove(entry.getKey()) ;
            editor.apply();
        }
        editor.commit() ;
    }

    public void removeAllData(List<QrGpsData> data){
        Log.d("map values", "Delete All Data");
        editor = sharedPreferences.edit();
        for (QrGpsData row: data) {
            editor.remove(row.getCustomId()) ;
            editor.apply();
        }
        editor.commit() ;
    }

    public String getData(String key){
        return sharedPreferences.getString(key, null);
    }

    public List<QrGpsData> getAll(){
        List<QrGpsData> qrGpsData =null;
        QrGpsData value;
        Map<String, ?> allEntries = sharedPreferences.getAll();
        if(!allEntries.entrySet().isEmpty()){
            qrGpsData = new ArrayList<>();
            for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
                value = GsonHelper.getGsonBuilder().fromJson(entry.getValue().toString(), QrGpsData.class);
                qrGpsData.add(value);
            }
        }
        return qrGpsData;
    }

    public void showContent(){
        Map<String, ?> allEntries = sharedPreferences.getAll();
        Log.d("map values", "========================Valores almacenados=============================");
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
        }
        Log.d("map values", "===========================================================");
    }
}
