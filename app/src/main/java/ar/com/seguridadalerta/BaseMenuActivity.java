package ar.com.seguridadalerta;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.material.navigation.NavigationView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Date;

import ar.com.seguridadalerta.api.model.GenericApiResult;
import ar.com.seguridadalerta.api.model.LoginResult;
import ar.com.seguridadalerta.api.model.LoginValue;
import ar.com.seguridadalerta.api.model.Logout;
import ar.com.seguridadalerta.api.service.LogoutService;
import ar.com.seguridadalerta.api.service.UserService;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.observers.BlockingBaseObserver;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseMenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TURNO_ACTIVO_PREFERENCE = "TURNO_ACTIVO";
    Menu menu;
    SubMenu menuUsuarios;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        //check current user session if is not present redirect to login screen
        if(UserService.getInstance(BaseMenuActivity.this).getUser() == null) {
            redirectToLoginScreen();
        } else {

            menu = navigationView.getMenu();
            menuUsuarios = menu.addSubMenu(getString(R.string.usuarios_extra));

            MenuItem navUser = menu.findItem(R.id.nav_username);
            navUser.setTitle(String.format("%s, %s", UserService.getInstance(BaseMenuActivity.this).getUser().getValue().getSocio().getApellido(),
                    UserService.getInstance(BaseMenuActivity.this).getUser().getValue().getSocio().getNombre()));

            MenuItem navMultilogin = menu.findItem(R.id.nav_multilogin);
            navMultilogin.setEnabled(UserService.getInstance(BaseMenuActivity.this).getUser().getValue().getObjetivo().isMultilogin());
            actualizarMultipleLogins();
        }
    }

    void redirectToLoginScreen() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    void logout() {
        if(UserService.getInstance(BaseMenuActivity.this).getUser() != null) {
            LoginValue currentUser = UserService.getInstance(BaseMenuActivity.this).getUser().getValue();
            Observable<GenericApiResult> apiResultObservable = LogoutService.getInstance().logout(
                    new Logout(currentUser.getSocio().getIdSocio(),
                            currentUser.getObjetivo().getId(), currentUser.getCronoId()),
                    BaseMenuActivity.this);

            apiResultObservable
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(new BlockingBaseObserver<GenericApiResult>() {
                @Override
                public void onNext(GenericApiResult genericApiResult) {
                    Answers.getInstance().logCustom(new CustomEvent("Logout exitoso")
                            .putCustomAttribute("Fecha", new Date().toString())
                            .putCustomAttribute("Usuario", String.format("%s %s",
                                    currentUser.getSocio().getNombre(), currentUser.getSocio().getApellido())));
                    UserService.getInstance(BaseMenuActivity.this).setUser(null);
                    redirectToLoginScreen();
                }

                @Override
                public void onError(Throwable e) {
                    showError(getString(R.string.error_logout), e);
                }
            });
        }
    }

    private void showError(String message, Throwable e) {
        // display toast in short period of time
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
        if(e != null) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NotNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            logout();
        } else if (id == R.id.nav_novedades) {
            openActivity(NovedadActivity.class);
        } else if (id == R.id.nav_reporte_horas) {
            openActivity(ReporteHorasActivity.class);
        } else if (id == R.id.nav_home) {
            openActivity(MainActivity.class);
        } else if (id == R.id.nav_rondas) {
            openActivity(QRActivity.class);
        } else if (id == R.id.nav_multilogin) {
            openActivity(MultipleLoginActivity.class);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean(TURNO_ACTIVO_PREFERENCE, false).apply();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean(TURNO_ACTIVO_PREFERENCE, false).apply();
    }

    @Override
    public void onResume() {
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean(TURNO_ACTIVO_PREFERENCE, true).apply();
    }

    private void openActivity(Class activity) {
        Intent intent = new Intent(this, activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
    public void actualizarMultipleLogins() {
        menuUsuarios.clear();
        ArrayList<LoginResult> logins = UserService.getInstance(BaseMenuActivity.this).getMultipleLogins();
        for (LoginResult user : logins) {
            menuUsuarios.add(String.format("%s %s", user.getValue().getSocio().getNombre(), user.getValue().getSocio().getApellido()));
        }

    }
}
