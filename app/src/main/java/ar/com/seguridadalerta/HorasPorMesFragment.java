package ar.com.seguridadalerta;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import ar.com.seguridadalerta.api.model.HorasMesQuery;
import ar.com.seguridadalerta.api.model.HorasMesResult;
import ar.com.seguridadalerta.api.service.HorasMesService;
import ar.com.seguridadalerta.api.service.UserService;
import ar.com.seguridadalerta.api.util.DateUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.observers.BlockingBaseObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HorasPorMesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HorasPorMesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HorasPorMesFragment extends Fragment implements View.OnClickListener {
    private OnFragmentInteractionListener mListener;

    private ProgressBar spinner;
    private TextView textViewMes;
    private TextView textViewSemana1;
    private TextView textViewSemana2;
    private TextView textViewSemana3;
    private TextView textViewSemana4;
    private TextView textViewSemana5;
    private TextView textViewTotal;

    private Button buttonSemana1;
    private Button buttonSemana2;
    private Button buttonSemana3;
    private Button buttonSemana4;
    private Button buttonSemana5;


    public HorasPorMesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HorasPorMesFragment.
     */
    public static HorasPorMesFragment newInstance() {
        return new HorasPorMesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_horas_por_mes, container, false);

        spinner = view.findViewById(R.id.progressBar1);
        spinner.setIndeterminate(true);
        spinner.setVisibility(View.GONE);
        textViewMes = view.findViewById(R.id.horas_nombre_mes);
        textViewSemana1 = view.findViewById(R.id.horas_semana_1);
        textViewSemana2 = view.findViewById(R.id.horas_semana_2);
        textViewSemana3 = view.findViewById(R.id.horas_semana_3);
        textViewSemana4 = view.findViewById(R.id.horas_semana_4);
        textViewSemana5 = view.findViewById(R.id.horas_semana_5);
        textViewTotal = view.findViewById(R.id.horas_total);

        buttonSemana1 = view.findViewById(R.id.butttonSemana1);
        buttonSemana2 = view.findViewById(R.id.butttonSemana2);
        buttonSemana3 = view.findViewById(R.id.butttonSemana3);
        buttonSemana4 = view.findViewById(R.id.butttonSemana4);
        buttonSemana5 = view.findViewById(R.id.butttonSemana5);

        buttonSemana1.setOnClickListener(this);
        buttonSemana2.setOnClickListener(this);
        buttonSemana3.setOnClickListener(this);
        buttonSemana4.setOnClickListener(this);
        buttonSemana5.setOnClickListener(this);

        obtenerReporte();

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.butttonSemana1:
                ((ReporteHorasActivity) Objects.requireNonNull(getActivity())).mostrarSemana(1);
                break;
            case R.id.butttonSemana2:
                ((ReporteHorasActivity) Objects.requireNonNull(getActivity())).mostrarSemana(2);
                break;
            case R.id.butttonSemana3:
                ((ReporteHorasActivity) Objects.requireNonNull(getActivity())).mostrarSemana(3);
                break;
            case R.id.butttonSemana4:
                ((ReporteHorasActivity) Objects.requireNonNull(getActivity())).mostrarSemana(4);
                break;
            case R.id.butttonSemana5:
                ((ReporteHorasActivity) Objects.requireNonNull(getActivity())).mostrarSemana(5);
                break;
        }
    }

    private void obtenerReporte() {
        spinner.setVisibility(View.VISIBLE);
        Calendar calendar = Calendar.getInstance();
        Date currentDate = new Date();
        calendar.setTime(currentDate);
        HorasMesService.getInstance().getHoras(new HorasMesQuery(
                UserService.getInstance(getActivity()).getUser().getValue().getSocio().getIdSocio(),
                    calendar.get(Calendar.DATE),
                    calendar.get(Calendar.MONTH) + 1,
                    calendar.get(Calendar.YEAR)
                ), getContext())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(horasResultBlockingBaseObserver);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private BlockingBaseObserver<HorasMesResult> horasResultBlockingBaseObserver = new BlockingBaseObserver<HorasMesResult>() {
        @Override
        public void onNext(HorasMesResult horasMesResult) {
            spinner.setVisibility(View.INVISIBLE);
            Calendar curentDate = Calendar.getInstance();
            curentDate.setTime(new Date());

            textViewMes.setText(String.format(" %s", DateUtil.getNombreMes(curentDate.get(Calendar.MONTH))));

            textViewSemana1.setText(String.format("%s", horasMesResult.getValue().getSemana1()));
            textViewSemana2.setText(String.format("%s", horasMesResult.getValue().getSemana2()));
            textViewSemana3.setText(String.format("%s", horasMesResult.getValue().getSemana3()));
            textViewSemana4.setText(String.format("%s", horasMesResult.getValue().getSemana4()));
            textViewSemana5.setText(String.format("%s", horasMesResult.getValue().getSemana5()));
            textViewTotal.setText(String.format("%s", horasMesResult.getValue().getTotal()));
        }

        @Override
        public void onError(Throwable e) {
            try {
                showError(getString(R.string.error_horas_mes), e);
            } catch (IllegalStateException e1) {
                e.printStackTrace();
            }
        }
    };

    private void showError(String message, Throwable e) {
        // display toast in short period of time
        Activity activity = getActivity();
        if(isAdded() && activity != null) {
            Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
        }
        spinner.setVisibility(View.GONE);
        if(e != null) {
            e.printStackTrace();
        }
    }
}
