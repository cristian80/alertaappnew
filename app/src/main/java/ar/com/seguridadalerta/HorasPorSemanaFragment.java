package ar.com.seguridadalerta;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import ar.com.seguridadalerta.api.model.HorasSemanaQuery;
import ar.com.seguridadalerta.api.model.HorasSemanaResult;
import ar.com.seguridadalerta.api.model.HorasSemanaValue;
import ar.com.seguridadalerta.api.service.HorasSemanaService;
import ar.com.seguridadalerta.api.service.UserService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.observers.BlockingBaseObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HorasPorSemanaFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HorasPorSemanaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HorasPorSemanaFragment extends Fragment implements View.OnClickListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_SEMANA = "ARG_SEMANA";
    private static final String TAG = "HorasPorSemanaFragment";

    private int mSemana;

    private ProgressBar spinner;
    private TextView textViewSemana;

    private TextView horasDescripcionDia1;
    private TextView horasDescripcionDia2;
    private TextView horasDescripcionDia3;
    private TextView horasDescripcionDia4;
    private TextView horasDescripcionDia5;
    private TextView horasDescripcionDia6;
    private TextView horasDescripcionDia7;

    private TextView horasDia1;
    private TextView horasDia2;
    private TextView horasDia3;
    private TextView horasDia4;
    private TextView horasDia5;
    private TextView horasDia6;
    private TextView horasDia7;


    private FloatingActionButton buttonVolver;
    private TableLayout tableLayoutHorasSemana;




    private OnFragmentInteractionListener mListener;

    public HorasPorSemanaFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param paramSemana Parameter 1.
     * @return A new instance of fragment HorasPorSemanaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HorasPorSemanaFragment newInstance(int paramSemana) {
        HorasPorSemanaFragment fragment = new HorasPorSemanaFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SEMANA, paramSemana);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSemana = getArguments().getInt(ARG_SEMANA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_horas_por_semana, container, false);

        spinner = view.findViewById(R.id.progressBar1);
        spinner.setIndeterminate(true);
        spinner.setVisibility(View.GONE);
        textViewSemana = view.findViewById(R.id.horas_nombre_semana);
        textViewSemana.setText(String.format(" %s", mSemana));
        buttonVolver = view.findViewById(R.id.horas_volver);
        buttonVolver.setOnClickListener(this);

        horasDia1 = view.findViewById(R.id.horas_dia_1);
        horasDia2 = view.findViewById(R.id.horas_dia_2);
        horasDia3 = view.findViewById(R.id.horas_dia_3);
        horasDia4 = view.findViewById(R.id.horas_dia_4);
        horasDia5 = view.findViewById(R.id.horas_dia_5);
        horasDia6 = view.findViewById(R.id.horas_dia_6);
        horasDia7 = view.findViewById(R.id.horas_dia_7);

        horasDescripcionDia1 = view.findViewById(R.id.horas_descripcion_dia_1);
        horasDescripcionDia2 = view.findViewById(R.id.horas_descripcion_dia_2);
        horasDescripcionDia3 = view.findViewById(R.id.horas_descripcion_dia_3);
        horasDescripcionDia4 = view.findViewById(R.id.horas_descripcion_dia_4);
        horasDescripcionDia5 = view.findViewById(R.id.horas_descripcion_dia_5);
        horasDescripcionDia6 = view.findViewById(R.id.horas_descripcion_dia_6);
        horasDescripcionDia7 = view.findViewById(R.id.horas_descripcion_dia_7);

        tableLayoutHorasSemana = view.findViewById(R.id.tableLayoutHorasSemana);

        obtenerReporte();

        return view;
    }

    private void setTextoHoras(int index, float horas) {
        switch (index) {
            case 0:
                horasDia1.setText(String.format("%s", horas));
            case 1:
                horasDia2.setText(String.format("%s", horas));
            case 2:
                horasDia3.setText(String.format("%s", horas));
            case 3:
                horasDia4.setText(String.format("%s", horas));
            case 4:
                horasDia5.setText(String.format("%s", horas));
            case 5:
                horasDia6.setText(String.format("%s", horas));
            case 6:
                horasDia7.setText(String.format("%s", horas));
        }
    }

    private void setTextoDescripcion(int index, String descripcion) {
        switch (index) {
            case 0:
                horasDescripcionDia1.setText(String.format("%s", descripcion));
            case 1:
                horasDescripcionDia2.setText(String.format("%s", descripcion));
            case 2:
                horasDescripcionDia3.setText(String.format("%s", descripcion));
            case 3:
                horasDescripcionDia4.setText(String.format("%s", descripcion));
            case 4:
                horasDescripcionDia5.setText(String.format("%s", descripcion));
            case 5:
                horasDescripcionDia6.setText(String.format("%s", descripcion));
            case 6:
                horasDescripcionDia7.setText(String.format("%s", descripcion));
        }
    }

    private void obtenerReporte() {
        spinner.setVisibility(View.VISIBLE);
        Calendar calendar = Calendar.getInstance();
        Date currentDate = new Date();
        calendar.setTime(currentDate);
        HorasSemanaService.getInstance().getHoras(new HorasSemanaQuery(
                UserService.getInstance(getActivity()).getUser().getValue().getSocio().getIdSocio(),
                calendar.get(Calendar.DATE),
                calendar.get(Calendar.MONTH) +1,
                calendar.get(Calendar.YEAR),
                mSemana
        ), getContext())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(semanaResultBlockingBaseObserver);
    }

    private BlockingBaseObserver<HorasSemanaResult> semanaResultBlockingBaseObserver = new BlockingBaseObserver<HorasSemanaResult>() {
        @Override
        public void onNext(HorasSemanaResult horasSemanaResult) {
            spinner.setVisibility(View.INVISIBLE);
            mostrarHoras(horasSemanaResult);
        }

        @Override
        public void onError(Throwable e) {
            showError(getString(R.string.error_horas_mes), e);
        }
    };

    private void mostrarHoras(HorasSemanaResult horasSemanaResult) {

        if(horasSemanaResult.getValue() != null) {
            int index = 0;
            for (HorasSemanaValue item: horasSemanaResult.getValue()) {
                if(index < 7) {
                    setTextoDescripcion(index, item.getDia());
                    setTextoHoras(index, item.getHoras());
                } else {
                    break;
                }
                index++;
            }
        }
    }

    private void showError(String message, Throwable e) {
        // display toast in short period of time
        Toast.makeText(getActivity(),message,Toast.LENGTH_LONG).show();
        spinner.setVisibility(View.GONE);
        if(e != null) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.horas_volver) {
            ((ReporteHorasActivity) Objects.requireNonNull(getActivity())).mostrarMes();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
