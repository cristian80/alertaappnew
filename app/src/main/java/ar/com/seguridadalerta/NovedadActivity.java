package ar.com.seguridadalerta;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Rational;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.CameraX;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureConfig;
import androidx.camera.core.Preview;
import androidx.camera.core.PreviewConfig;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import ar.com.seguridadalerta.api.model.GenericApiResult;
import ar.com.seguridadalerta.api.model.LoginResult;
import ar.com.seguridadalerta.api.model.Novedad;
import ar.com.seguridadalerta.api.service.FileUploadService;
import ar.com.seguridadalerta.api.service.NovedadService;
import ar.com.seguridadalerta.api.service.UserService;
import ar.com.seguridadalerta.api.util.Sha1Hex;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.observers.BlockingBaseObserver;
import io.reactivex.schedulers.Schedulers;

public class NovedadActivity extends BaseMenuActivity implements View.OnClickListener {

    private static final String TAG = "NovedadActivity";

    private CameraX.LensFacing lensFacing = CameraX.LensFacing.BACK;
    private TextureView texture;
    private ImageCapture imageCapture;
    private static final int PHOTO_WIDTH = 500;
    private static final int PHOTO_HEIGTH = 500;
    private ProgressBar spinner;
    private TextView textDescripcion;
    private FloatingActionButton fabTomarFoto;
    private CheckBox checkBoxTieneFoto;
    private File foto;
    private String fotoFileName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_novedad);
        super.onCreate(savedInstanceState);

        spinner = findViewById(R.id.progressBar1);
        spinner.setIndeterminate(true);
        spinner.setVisibility(View.GONE);
        textDescripcion = findViewById(R.id.textNovedadesDescripcion);
        textDescripcion.setImeOptions(EditorInfo.IME_ACTION_DONE);
        textDescripcion.setRawInputType(InputType.TYPE_CLASS_TEXT);
        texture = findViewById(R.id.camera);
        Button buttonGrabar = findViewById(R.id.buttonNovedadesGrabar);
        fabTomarFoto = findViewById(R.id.fabNovedadesFoto);
        checkBoxTieneFoto = findViewById(R.id.checkTieneFoto);

        buttonGrabar.setOnClickListener(this);
        fabTomarFoto.setOnClickListener(v -> tomarFoto());

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // Do the file write
            Log.d(TAG, "permission is granted");
        } else {
            // Request permission from the user
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
                            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        }


        texture.post(this::startCamera);

    }

    private void startCamera() {
        DisplayMetrics metrics = new DisplayMetrics();
        texture.getDisplay().getRealMetrics(metrics);
        Size screenSize = new Size(PHOTO_WIDTH, PHOTO_HEIGTH);
        Rational screenAspectRatio = new Rational(PHOTO_WIDTH, PHOTO_HEIGTH);
        PreviewConfig previewConfig = new PreviewConfig.Builder()
                .setLensFacing(lensFacing)
                .setTargetResolution(screenSize)
                .setTargetAspectRatio(screenAspectRatio)
                .setTargetRotation(getWindowManager().getDefaultDisplay().getRotation())
                .setTargetRotation(texture.getDisplay().getRotation())
                .build();
        Preview preview = new Preview(previewConfig);
        preview.setOnPreviewOutputUpdateListener(output -> {
            ViewGroup parent = (ViewGroup) texture.getParent();
            parent.removeView(texture);
            texture.setSurfaceTexture(output.getSurfaceTexture());
            parent.addView(texture, 0);
            updateTransform();
        });
        // Create configuration object for the image capture use case
        ImageCaptureConfig imageCaptureConfig = new ImageCaptureConfig.Builder()
                .setLensFacing(lensFacing)
                .setTargetAspectRatio(new Rational(PHOTO_WIDTH, PHOTO_HEIGTH))
                .setTargetResolution(new Size(PHOTO_WIDTH, PHOTO_HEIGTH))
                .setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
                .setTargetRotation(Surface.ROTATION_270)
                .build();
        // Build the image capture use case and attach button click listener
        imageCapture = new ImageCapture(imageCaptureConfig);
        new Handler(Looper.getMainLooper()).post(() ->
                CameraX.bindToLifecycle(NovedadActivity.this, preview, imageCapture));
    }

    private void updateTransform() {
        Matrix matrix = new Matrix();
        float centerX = texture.getWidth() / 2f;
        float centerY = texture.getHeight() / 2f;

        int rotationDegrees = 0;

        switch (texture.getDisplay().getRotation()) {
            case Surface.ROTATION_0:
                rotationDegrees = 0;
                break;
            case Surface.ROTATION_90:
                rotationDegrees = 90;
                break;
            case Surface.ROTATION_180:
                rotationDegrees = 180;
                break;
            case Surface.ROTATION_270:
                rotationDegrees = 270;
                break;
            default:
                break;
        }
        matrix.postRotate(-rotationDegrees, centerX, centerY);
        texture.setTransform(matrix);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.buttonNovedadesGrabar) {
            grabarNovedad();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void tomarFoto() {
        spinner.setVisibility(View.VISIBLE);
        checkBoxTieneFoto.setChecked(false);
        try {
            fotoFileName = Sha1Hex.makeSHA1Hash(new Date().toString())+ ".jpg";
        } catch (NoSuchAlgorithmException e) {
            showError(getString(R.string.error_login_data), e);
            return;
        } catch (UnsupportedEncodingException e) {
            showError(getString(R.string.error_login_data), e);
            return;
        }
        foto = new File(Environment.getExternalStorageDirectory(), fotoFileName );
        imageCapture.takePicture(foto, new ImageCapture.OnImageSavedListener() {
            @Override
            public void onImageSaved(@NonNull File file) {
                checkBoxTieneFoto.setChecked(true);
                LoginResult loginResult = UserService.getInstance(NovedadActivity.this).getUser();
                //store image on amazon
                FileUploadService.getInstance(NovedadActivity.this)
                    .uploadFile("images/novedades/", fotoFileName, foto,NovedadActivity.this,
                        new TransferListener() {
                            @Override
                            public void onStateChanged(int id, TransferState state) {
                                if (TransferState.COMPLETED == state) {
                                    Log.e(TAG, "Image Saved on S3");
                                    showError(getString(R.string.image_upload_success), null);
                                }
                            }

                            @Override
                            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                                Log.d(TAG, bytesCurrent + " of " + bytesTotal);
                            }

                            @Override
                            public void onError(int id, Exception ex) {
                                showError(getString(R.string.error_image_upload), ex);
                            }
                        }, loginResult.getAmz_secret(), loginResult.getAmz_key());

            }
            @Override
            public void onError(@NonNull ImageCapture.ImageCaptureError imageCaptureError, @NonNull String message, @Nullable Throwable cause) {
                assert cause != null;
                showError(getString(R.string.error_capture_image), cause);
            }
        });

    }

    private void grabarNovedad() {
        if(textDescripcion.getText().toString().length() == 0) {
            textDescripcion.setError("La descripción no puede estar vacía");
            return;
        }
        spinner.setVisibility(View.VISIBLE);
        LoginResult user = UserService.getInstance(NovedadActivity.this).getUser();
        Novedad novedad = new Novedad();
        novedad.setDescripcion(textDescripcion.getText().toString());
        novedad.setIdObjetivo(user.getValue().getObjetivo().getId());
        novedad.setIdSocio(user.getValue().getSocio().getIdSocio());
        novedad.setImagenUrl( foto != null && fotoFileName.length() > 0 && checkBoxTieneFoto.isChecked() ? fotoFileName : "" );
        grabarNovedad(novedad);

    }

    private void grabarNovedad(Novedad novedad) {
        NovedadService.getInstance().postNovedad(novedad, NovedadActivity.this)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new BlockingBaseObserver<GenericApiResult>() {
                    @Override
                    public void onNext(GenericApiResult genericApiResult) {
                        spinner.setVisibility(View.INVISIBLE);
                        foto = null;
                        fotoFileName ="";
                        textDescripcion.setText("");
                        checkBoxTieneFoto.setChecked(false);
                        Toast.makeText(getApplicationContext(), R.string.datos_grabados,Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(Throwable e) {
                        showError(getString(R.string.error_novedad), e);
                    }
                });
    }

    private void showError(String message, Throwable e) {
        // display toast in short period of time
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
        spinner.setVisibility(View.GONE);
        if(e != null) {
            e.printStackTrace();
        }
    }
}
