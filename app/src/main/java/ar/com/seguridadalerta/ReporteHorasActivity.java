package ar.com.seguridadalerta;

import android.net.Uri;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.fragment.app.FragmentTransaction;

public class ReporteHorasActivity extends BaseMenuActivity implements HorasPorMesFragment.OnFragmentInteractionListener, HorasPorSemanaFragment.OnFragmentInteractionListener {

    private static final String TAG = "ReporteHorasActivity";

    private FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_reporte_horas);
        super.onCreate(savedInstanceState);
        container = findViewById(R.id.container);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        HorasPorMesFragment horasPorMesFragment = HorasPorMesFragment.newInstance();
        ft.add(container.getId(), horasPorMesFragment).commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public void mostrarSemana(int semana) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        HorasPorSemanaFragment horasPorSemanaFragment = HorasPorSemanaFragment.newInstance(semana);
        ft.replace(container.getId(), horasPorSemanaFragment).commit();
    }

    public void mostrarMes() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        HorasPorMesFragment horasPorMesFragment = HorasPorMesFragment.newInstance();
        ft.replace(container.getId(), horasPorMesFragment).commit();
    }
}
