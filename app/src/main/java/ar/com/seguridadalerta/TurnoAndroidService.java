package ar.com.seguridadalerta;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import ar.com.seguridadalerta.api.model.GenericApiResult;
import ar.com.seguridadalerta.api.model.LoginResult;
import ar.com.seguridadalerta.api.model.LoginValue;
import ar.com.seguridadalerta.api.model.Logout;
import ar.com.seguridadalerta.api.service.LogoutService;
import ar.com.seguridadalerta.api.service.UserService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.observers.BlockingBaseObserver;
import io.reactivex.schedulers.Schedulers;

public class TurnoAndroidService extends Service {
    public static final int NOTIFICATION_ID = 111;
    private static final String CHANNEL_ID = "my_channel_01";
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    private static final String TAG = "TurnoAndroidService";

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            // Normally we would do some work here, like download a file.

            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                stopForeground(true);
            } else {
                stopSelf(msg.arg1);
            }
        }
    }

    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Notification notification = new NotificationCompat.Builder(this, getNotificationChannelId(CHANNEL_ID, TAG))
                    .setContentTitle("")
                    .setContentText("").build();
            startForeground(NOTIFICATION_ID, notification);
        }

    }

    private String notificationChannel = "";
    @RequiresApi(Build.VERSION_CODES.O)
    private String getNotificationChannelId(String channelId, String channelName) {
        if(notificationChannel.length() == 0) {
            NotificationChannel chan = new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_NONE);

            chan.setLightColor(Color.BLUE);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            service.createNotificationChannel(chan);
            notificationChannel = channelId;
        }
        return notificationChannel;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "service starting");
        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification notification = new NotificationCompat.Builder(this, getNotificationChannelId(CHANNEL_ID, TAG))
                    .setContentTitle("")
                    .setContentText("").build();
            startForeground(NOTIFICATION_ID, notification);
        }

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                //Verifico que el turno no deba ser cerrado
                if(UserService.getInstance(getApplicationContext()).getUser() != null) {
                    LoginValue currentUser = UserService.getInstance(getApplicationContext()).getUser().getValue();
                    if(currentUser.isTurnoCerrado()) {
                        UserService.getInstance(getApplicationContext()).setUser(null);
                        LogoutService.getInstance().logout(
                                new Logout(
                                        currentUser.getSocio().getIdSocio(),
                                        currentUser.getObjetivo().getId(),
                                        currentUser.getCronoId()
                                ), getApplicationContext())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.newThread())
                                .subscribe(new BlockingBaseObserver<GenericApiResult>() {
                                    @Override
                                    public void onNext(GenericApiResult genericApiResult) {
                                        // si la app esta activa cierro la pantalla y vuelvo al login)
                                        if(PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                                                .getBoolean(BaseMenuActivity.TURNO_ACTIVO_PREFERENCE, false)) {
                                            Answers.getInstance().logCustom(new CustomEvent("Cierre automático de turno")
                                                    .putCustomAttribute("Fecha", new Date().toString())
                                                    .putCustomAttribute("Usuario", String.format("%s %s",
                                                            currentUser.getSocio().getNombre(), currentUser.getSocio().getApellido())));
                                            Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                            loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(loginIntent);
                                        }
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.e(TAG, e.getMessage());
                                        e.printStackTrace();
                                    }
                                });
                    }
                } else {
                    Log.e(TAG, "No User Found.");
                }

                //verifico los turnos de los usuarios de multilogin
                ArrayList<LoginResult> logins = UserService.getInstance(getApplicationContext()).getMultipleLogins();
                for (LoginResult user : logins) {
                    if(user.getValue().isTurnoCerrado()) {
                        UserService.getInstance(getApplicationContext()).removeMultipleLogin(user);
                    }
                }

            }
        },0,60000);//Update every 1 minute

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "service done");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        // TODO Auto-generated method stub
        Intent restartService = new Intent(getApplicationContext(), this.getClass());
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);

        //Restart the service once it has been killed android
        AlarmManager alarmService = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() +100, restartServicePI);
    }
}
