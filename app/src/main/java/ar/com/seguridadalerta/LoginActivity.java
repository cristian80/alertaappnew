package ar.com.seguridadalerta;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Rational;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraX;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureConfig;
import androidx.camera.core.Preview;
import androidx.camera.core.PreviewConfig;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Objects;

import ar.com.seguridadalerta.api.model.LoginResult;
import ar.com.seguridadalerta.api.model.LoginUser;
import ar.com.seguridadalerta.api.service.FileUploadService;
import ar.com.seguridadalerta.api.service.LoginService;
import ar.com.seguridadalerta.api.service.UserService;
import ar.com.seguridadalerta.api.util.VersionUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.observers.BlockingBaseObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "LoginActivity";

    private CameraX.LensFacing lensFacing = CameraX.LensFacing.FRONT;
    private TextureView texture;
    private ImageCapture imageCapture;

    private static final int PHOTO_WIDTH = 400;
    private static final int PHOTO_HEIGTH = 300;

    private static final int PHOTO_PRVIEW_WIDTH = 120;
    private static final int PHOTO_PRVIEW_HEIGTH = 100;

    private TextView textUser;
    private TextView textPassword;
    private ProgressBar spinner;
    private LocationTrack locationTrack;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginButton = findViewById(R.id.buttonLoginLogin);
        loginButton.setOnClickListener(this);
        spinner = findViewById(R.id.progressBar1);
        spinner.setIndeterminate(true);
        spinner.setVisibility(View.GONE);
        textUser = findViewById(R.id.textLoginUser);
        textPassword = findViewById(R.id.textPassword);
        texture = findViewById(R.id.camera);
        locationTrack = new LocationTrack();
        locationTrack.getGpsLocation(LoginActivity.this);
        TextView textVersion = findViewById(R.id.version);
        textVersion.setText(String.format("Versión: %s", VersionUtil.getVersion(LoginActivity.this)));

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "permission is granted");
            texture.post(this::startCamera);
        } else {
            // Request permission from the user
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
                            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        }

    }


    private void startCamera() {
        try {
            DisplayMetrics metrics = new DisplayMetrics();
            texture.getDisplay().getRealMetrics(metrics);
            Size screenSize = new Size(PHOTO_PRVIEW_WIDTH, PHOTO_PRVIEW_HEIGTH);
            Rational screenAspectRatio = new Rational(PHOTO_PRVIEW_WIDTH, PHOTO_PRVIEW_HEIGTH);
            PreviewConfig previewConfig = new PreviewConfig.Builder()
                    .setLensFacing(lensFacing)
                    .setTargetResolution(screenSize)
                    .setTargetAspectRatio(screenAspectRatio)
                    .setTargetRotation(getWindowManager().getDefaultDisplay().getRotation())
                    .setTargetRotation(texture.getDisplay().getRotation())
                    .build();
            Preview preview = new Preview(previewConfig);
            preview.setOnPreviewOutputUpdateListener(output -> {
                ViewGroup parent = (ViewGroup) texture.getParent();
                parent.removeView(texture);
                texture.setSurfaceTexture(output.getSurfaceTexture());
                parent.addView(texture, 0);
                updateTransform();
            });
            // Create configuration object for the image capture use case
            ImageCaptureConfig imageCaptureConfig = new ImageCaptureConfig.Builder()
                    .setLensFacing(lensFacing)
                    .setTargetAspectRatio(new Rational(PHOTO_WIDTH, PHOTO_HEIGTH))
                    .setTargetResolution(new Size(PHOTO_WIDTH, PHOTO_HEIGTH))
                    .setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
                    .setTargetRotation(Surface.ROTATION_0)
                    .build();
            // Build the image capture use case and attach button click listener
            imageCapture = new ImageCapture(imageCaptureConfig);
            /*
            https://developer.android.com/jetpack/androidx/releases/camera
            Enforce calling the following methods on the Main (UI) thread, throwing an IllegalStateException when they are not. Future versions will allow usage on other threads and ensure serialization.
            CameraX.bindToLifecycle()
            CameraX.unbind()
            CameraX.unbindAll()
            ImageAnalysis.setAnalyzer()
            ImageAnalysis.getAnalyzer()
            ImageAnalysis.removeAnalyzer()
            Preview.removePreviewOutputListener()
            Preview.getOnPreviewOutputUpdateListener()
            Preview.setOnPreviewOutputUpdateListener()
            * */
            new Handler(Looper.getMainLooper()).post(() ->
                    CameraX.bindToLifecycle(LoginActivity.this, preview, imageCapture));
        } catch (Exception e) {
            showError(e.getMessage(), e);
        }
    }

    private void updateTransform() {
        Matrix matrix = new Matrix();
        float centerX = texture.getWidth() / 2f;
        float centerY = texture.getHeight() / 2f;

        int rotationDegrees = 0;

        switch (texture.getDisplay().getRotation()) {
            case Surface.ROTATION_0:
                rotationDegrees = 0;
                break;
            case Surface.ROTATION_90:
                rotationDegrees = 90;
                break;
            case Surface.ROTATION_180:
                rotationDegrees = 180;
                break;
            case Surface.ROTATION_270:
                rotationDegrees = 270;
                break;
            default:
                break;
        }
        matrix.postRotate(-rotationDegrees, centerX, centerY);
        texture.setTransform(matrix);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationTrack.stopListener(LoginActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationTrack.stopListener(LoginActivity.this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int index = 0;
        for (String permission:permissions) {
            Log.e(TAG, "permission: " + permission + " result " + grantResults[index]);
            if(permission.equals(Manifest.permission.CAMERA)) {
                if(grantResults[index] == 0) {
                    texture.post(this::startCamera);
                }
            }
            index++;
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.buttonLoginLogin) {
            getLoginInformation();
        }
    }

    private void showError(String message, Throwable e) {
        // display toast in short period of time
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
        spinner.setVisibility(View.GONE);
        loginButton.setEnabled(true);
        Log.d(TAG, ">>>>>>>>>> START LOGIN <<<<<<<<");
        if(e != null) {
            e.printStackTrace();
        }
    }

    public void getLoginInformation() {
        spinner.setVisibility(View.VISIBLE);
        loginButton.setEnabled(false);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            // Do the file write
            File file = new File(Environment.getExternalStorageDirectory(), "login_picture.jpg");
            if (imageCapture != null) {
                imageCapture.takePicture(file, new ImageCapture.OnImageSavedListener() {
                    @Override
                    public void onImageSaved(@NonNull File file) {
                        login(file);
                    }

                    @Override
                    public void onError(@NonNull ImageCapture.ImageCaptureError imageCaptureError, @NonNull String message, @Nullable Throwable cause) {
                        assert cause != null;
                        showError(getString(R.string.error_capture_image), cause);
                    }
                });
            } else {
                // Request permission from the user
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 0);
                showError(getString(R.string.error_capture_image_permission), null);
            }
        } else {
            showError(getString(R.string.error_camara_no_disponible), null);
        }
    }


    private void uploadUserLoginPhoto(String fileName, File file, LoginResult loginResult) {
        FileUploadService.getInstance(LoginActivity.this)
                .uploadFile("images/login/", fileName, file, LoginActivity.this,
                new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            Log.e(TAG, "Image Saved on S3");
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        Log.d(TAG, bytesCurrent + " of " + bytesTotal);
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        showError(getString(R.string.error_image_upload), ex);
                    }
                }, loginResult.getAmz_secret(), loginResult.getAmz_key());
    }

    @SuppressLint("CheckResult")
    private void login(File file) {

        String[] location = locationTrack.getGpsLocation(LoginActivity.this);
        if(location != null) {
            String coordenadas = String.format("%s, %s", location[0], location[1]);

            LoginUser loginUser = new LoginUser(textUser.getText().toString().toLowerCase(),
                    textPassword.getText().toString(),
                    "",
                    coordenadas);

            Log.e(TAG, "user: " + loginUser.getUserName() + " pass: " + loginUser.getPassword());

            LoginService.getInstance().login(loginUser, LoginActivity.this)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(new BlockingBaseObserver<LoginResult>() {
                        @Override
                        public void onNext(LoginResult loginResult) {
                            //set current user session
                            UserService.getInstance(LoginActivity.this).setUser(loginResult);
                            //upload foto to s3 on the background
                            uploadUserLoginPhoto(String.format("%s", loginResult.getValue().getLoginId()), file, loginResult);
                            Answers.getInstance().logCustom(new CustomEvent("Login exitoso")
                                    .putCustomAttribute("Fecha", new Date().toString())
                                    .putCustomAttribute("Usuario", String.format("%s %s",
                                            loginResult.getValue().getSocio().getNombre(),
                                            loginResult.getValue().getSocio().getApellido())));
                            //start Main activity
                            Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(mainIntent);
                        }
                        @Override
                        public void onError(Throwable e) {
                            if(e instanceof HttpException) {
                                HttpException error = (HttpException) e;
                                try {
                                    String errorBody = Objects.requireNonNull(error.response().errorBody()).string();
                                    JSONObject jObjError = new JSONObject(errorBody);
                                    showError(jObjError.getString("message"), e);
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            } else {
                                showError(e.getMessage(), e);
                            }
                        }
                    });
        } else {
            showError(getString(R.string.error_coordenadas), null);
        }
    }
}
