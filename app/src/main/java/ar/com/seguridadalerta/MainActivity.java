package ar.com.seguridadalerta;

import android.os.Bundle;
import android.widget.TextView;

import java.text.DateFormat;

import ar.com.seguridadalerta.api.model.LoginResult;
import ar.com.seguridadalerta.api.service.UserService;
import ar.com.seguridadalerta.api.util.VersionUtil;

public class MainActivity extends BaseMenuActivity {

    private static final String TAG = "MainActivity";

    private TextView textViewInicio;
    private TextView textViewFin;
    private TextView textSocio;
    private TextView textObjetivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        textViewFin = findViewById(R.id.textFinTurno);
        textViewInicio = findViewById(R.id.textInicioTurno);
        textSocio = findViewById(R.id.textSocio);
        textObjetivo = findViewById(R.id.textObjetivo);
        TextView textVersion = findViewById(R.id.version);
        textVersion.setText(String.format("Versión: %s", VersionUtil.getVersion(MainActivity.this)));
        mostrarHoras();
    }

    private void mostrarHoras() {
        LoginResult currentUser = UserService.getInstance(MainActivity.this).getUser();
        if(currentUser != null) {
            textViewInicio.setText(DateFormat.getInstance().format(currentUser.getValue().getDesdeFecha()));
            textViewFin.setText(DateFormat.getInstance().format(currentUser.getValue().getHastaFecha()));
            textSocio.setText(String.format("%s, %s", currentUser.getValue().getSocio().getApellido(), currentUser.getValue().getSocio().getNombre()));
            textObjetivo.setText(String.format("%s", currentUser.getValue().getObjetivo().getNombre()));
        }
    }
}
