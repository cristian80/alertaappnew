package ar.com.seguridadalerta.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ar.com.seguridadalerta.api.util.StringUtil;

public class GsonHelper {
    private static Gson gson;
    public static Gson getGsonBuilder() {

        if(gson == null) {
            JsonSerializer<Date> ser = (src, typeOfSrc, context) -> src == null ? null : new JsonPrimitive(src.getTime());

            JsonDeserializer<Date> deser = (json, typeOfT, context) -> {
                String value = json.getAsString();
                if (StringUtil.isNumeric(value)) {
                    return new Date(json.getAsLong());
                } else {
                    DateFormat myFormat = SimpleDateFormat.getDateTimeInstance();
                    try {
                        return myFormat.parse(value);
                    } catch (ParseException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };

            gson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, ser)
                    .registerTypeAdapter(Date.class, deser)
                    .create();
        }
        return gson;
    }
}
