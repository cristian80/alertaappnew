package ar.com.seguridadalerta.api.model;

import java.io.Serializable;
import java.util.Objects;

public class Objetivo implements Serializable {

    public Objetivo() {
    }

    public Objetivo(long id, String nombre, String telefCorporativo, boolean multilogin) {
        this.id = id;
        this.nombre = nombre;
        this.telefCorporativo = telefCorporativo;
        this.multilogin = multilogin;
    }

    private long id;
    private String nombre;
    private String telefCorporativo;
    private boolean multilogin;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefCorporativo() {
        return telefCorporativo;
    }

    public void setTelefCorporativo(String telefCorporativo) {
        this.telefCorporativo = telefCorporativo;
    }

    public boolean isMultilogin() {
        return multilogin;
    }

    public void setMultilogin(boolean multilogin) {
        this.multilogin = multilogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Objetivo)) return false;
        Objetivo objetivo = (Objetivo) o;
        return id == objetivo.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


}
