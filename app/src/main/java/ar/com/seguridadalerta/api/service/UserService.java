package ar.com.seguridadalerta.api.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ar.com.seguridadalerta.api.GsonHelper;
import ar.com.seguridadalerta.api.model.LoginResult;

public class UserService {

    private SharedPreferences preferences;
    private static final String PREFERENCE_USER = "PREFERENCE_USER";
    private static final String PREFERENCE_MULTIPLE_LOGIN = "PREFERENCE_MULTIPLE_LOGIN";

    private UserService(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    private static UserService userService;

    public static UserService getInstance(Context context) {
        if(userService == null) {
            userService = new UserService(context);
        }
        return userService;
    }

    public void setUser(LoginResult user) {
        if(user != null) {
            preferences.edit().putString(PREFERENCE_USER, GsonHelper.getGsonBuilder().toJson(user)).apply();
        } else {
            preferences.edit().remove(PREFERENCE_USER).apply();
        }
    }

    public ArrayList<LoginResult> getMultipleLogins() {
        Type listType = new TypeToken<ArrayList<LoginResult>>(){}.getType();
        ArrayList<LoginResult> logins = GsonHelper.getGsonBuilder().fromJson(preferences.getString(PREFERENCE_MULTIPLE_LOGIN, ""), listType);
        if(logins == null) logins = new ArrayList<>();
        return logins;
    }

    public void addMultipleLogin(LoginResult login) {
        ArrayList<LoginResult> logins = getMultipleLogins();
        if(!logins.contains(login)) {
            logins.add(login);
            preferences.edit().putString(PREFERENCE_MULTIPLE_LOGIN, GsonHelper.getGsonBuilder().toJson(logins)).apply();
        }
    }

    public void removeMultipleLogin(LoginResult login) {
        List logins = getMultipleLogins();
        if(logins != null && logins.contains(login)) {
            logins.remove(login);
            preferences.edit().putString(PREFERENCE_MULTIPLE_LOGIN, GsonHelper.getGsonBuilder().toJson(logins)).apply();
        }
    }

    public LoginResult getUser() {
        String jsonUser = preferences.getString(PREFERENCE_USER,"");
        LoginResult user = null;
        if(jsonUser.length() > 0) {
            user = GsonHelper.getGsonBuilder().fromJson(jsonUser, LoginResult.class);
        }
        return user;
    }

}
