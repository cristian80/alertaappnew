package ar.com.seguridadalerta.api.model;

import java.io.Serializable;
import java.util.Objects;

public class HorasSemanaQuery implements Serializable {

    public HorasSemanaQuery() {
    }

    public HorasSemanaQuery(long socioId, int dia, int mes, int anio, int semana) {
        this.socioId = socioId;
        this.dia = dia;
        this.mes = mes;
        this.anio = anio;
        this.semana = semana;
    }

    private long socioId;
    private int dia;
    private int mes;
    private int anio;
    private int semana;

    public long getSocioId() {
        return socioId;
    }

    public void setSocioId(long socioId) {
        this.socioId = socioId;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getSemana() {
        return semana;
    }

    public void setSemana(int semana) {
        this.semana = semana;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HorasSemanaQuery)) return false;
        HorasSemanaQuery horasMesQuery = (HorasSemanaQuery) o;
        return socioId == horasMesQuery.socioId &&
                dia == horasMesQuery.dia &&
                mes == horasMesQuery.mes &&
                anio == horasMesQuery.anio&&
                semana == horasMesQuery.semana;
    }

    @Override
    public int hashCode() {
        return Objects.hash(socioId, dia, mes, anio);
    }

    @Override
    public String toString() {
        return "HorasMesQuery{" +
                "socioId=" + socioId +
                ", dia=" + dia +
                ", mes=" + mes +
                ", anio=" + anio +
                ", semana=" + semana +
                '}';
    }
}
