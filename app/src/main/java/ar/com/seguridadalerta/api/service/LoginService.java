package ar.com.seguridadalerta.api.service;

import android.content.Context;

import ar.com.seguridadalerta.api.LoginApi;
import ar.com.seguridadalerta.api.NetworkClient;
import ar.com.seguridadalerta.api.model.LoginResult;
import ar.com.seguridadalerta.api.model.LoginUser;
import io.reactivex.Observable;
import retrofit2.Retrofit;

public class LoginService {

    private static LoginService loginService;

    public static LoginService getInstance() {
        if(loginService == null) {
            loginService = new LoginService();
        }
        return loginService;
    }

    public Observable<LoginResult> login(LoginUser loginUser, Context context) {
        Observable<LoginResult> loginResult;
        Retrofit retrofit = NetworkClient.getRetrofitClient(context);
        LoginApi loginApi = retrofit.create(LoginApi.class);
        loginResult = loginApi.login(loginUser);
        return loginResult;
    }
}
