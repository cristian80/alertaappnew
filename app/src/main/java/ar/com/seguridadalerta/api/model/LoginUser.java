package ar.com.seguridadalerta.api.model;

import java.io.Serializable;

public class LoginUser implements Serializable {

    @Override
    public int hashCode() {
        return (userName + password).hashCode();
    }

    private String userName;
    private String password;
    private String imagenUrl;
    private String coordenadas;

    public LoginUser() {

    }

    public LoginUser(String userName, String password, String imagenUrl, String coordenadas) {
        this.userName = userName;
        this.password = password;
        this.imagenUrl = imagenUrl;
        this.coordenadas = coordenadas;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImagenUrl() {
        return imagenUrl;
    }

    public void setImagenUrl(String imagenUrl) {
        this.imagenUrl = imagenUrl;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }
}
