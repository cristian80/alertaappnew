package ar.com.seguridadalerta.api.service;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;

public class TransferObserverContainer {

    public TransferObserverContainer(TransferObserver transferObserver, String fileURL) {
        this.fileURL = fileURL;
        this.transferObserver = transferObserver;
    }

    private TransferObserver transferObserver;
    private String fileURL;

    public TransferObserver getTransferObserver() {
        return transferObserver;
    }

    public String getFileURL() {
        return fileURL;
    }
}
