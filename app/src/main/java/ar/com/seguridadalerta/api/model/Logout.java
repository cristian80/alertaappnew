package ar.com.seguridadalerta.api.model;

import java.io.Serializable;
import java.util.Objects;

public class Logout implements Serializable {

    public Logout() {
    }

    public Logout(long idSocio, long idObjetivo, long idCronograma) {
        this.idSocio = idSocio;
        this.idObjetivo = idObjetivo;
        this.idCronograma = idCronograma;
    }

    private long idSocio;
    private long idObjetivo;
    private long idCronograma;

    public long getIdSocio() {
        return idSocio;
    }

    public void setIdSocio(long idSocio) {
        this.idSocio = idSocio;
    }

    public long getIdObjetivo() {
        return idObjetivo;
    }

    public void setIdObjetivo(long idObjetivo) {
        this.idObjetivo = idObjetivo;
    }

    public long getIdCronograma() {
        return idCronograma;
    }

    public void setIdCronograma(long idCronograma) {
        this.idCronograma = idCronograma;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Logout)) return false;
        Logout logout = (Logout) o;
        return idSocio == logout.idSocio &&
                idObjetivo == logout.idObjetivo &&
                idCronograma == logout.idCronograma;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idSocio, idObjetivo, idCronograma);
    }

    @Override
    public String toString() {
        return "Logout{" +
                "idSocio='" + idSocio + '\'' +
                ", idObjetivo='" + idObjetivo + '\'' +
                ", idCronograma='" + idCronograma + '\'' +
                '}';
    }
}
