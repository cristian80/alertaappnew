package ar.com.seguridadalerta.api.model;

import java.io.Serializable;
import java.util.Objects;

public class LoginResult implements ApiResult, Serializable {

    public LoginResult() {
    }

    public LoginResult(String token, LoginValue value, String message, String timestamp, String status) {
        this.token = token;
        this.value = value;
        this.message = message;
        this.timestamp = timestamp;
        this.status = status;
    }

    private String token;
    private LoginValue value;
    private String message;
    private String timestamp;
    private String status;
    private String amz_key;
    private String amz_secret;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LoginValue getValue() {
        return value;
    }

    public void setValue(LoginValue value) {

        this.value = value;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getTimestamp() {
        return timestamp;
    }

    @Override
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoginResult)) return false;
        LoginResult that = (LoginResult) o;
        return token.equals(that.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token);
    }

    @Override
    public String getAmz_key() {
        return amz_key;
    }

    @Override
    public void setAmz_key(String amz_key) {
        this.amz_key = amz_key;
    }

    @Override
    public String getAmz_secret() {
        return amz_secret;
    }

    @Override
    public void setAmz_secret(String amz_secret) {
        this.amz_secret = amz_secret;
    }
}
