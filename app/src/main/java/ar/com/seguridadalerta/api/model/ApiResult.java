package ar.com.seguridadalerta.api.model;

public interface ApiResult {

    public String getStatus();

    public void setStatus(String status);

    public String getMessage();

    public void setMessage(String message);

    public String getTimestamp();

    public void setTimestamp(String timestamp);

    public String getAmz_key();

    public void setAmz_key(String amz_key);

    public String getAmz_secret();

    public void setAmz_secret(String amz_secret);

}
