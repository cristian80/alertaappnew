package ar.com.seguridadalerta.api.model;

import java.io.Serializable;
import java.util.Objects;

public class HorasMesQuery implements Serializable {

    public HorasMesQuery() {
    }

    public HorasMesQuery(long socioId, int dia, int mes, int anio) {
        this.socioId = socioId;
        this.dia = dia;
        this.mes = mes;
        this.anio = anio;
    }

    private long socioId;
    private int dia;
    private int mes;
    private int anio;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HorasMesQuery)) return false;
        HorasMesQuery horasMesQuery = (HorasMesQuery) o;
        return socioId == horasMesQuery.socioId &&
                dia == horasMesQuery.dia &&
                mes == horasMesQuery.mes &&
                anio == horasMesQuery.anio;
    }

    @Override
    public int hashCode() {
        return Objects.hash(socioId, dia, mes, anio);
    }

    @Override
    public String toString() {
        return "HorasMesQuery{" +
                "socioId=" + socioId +
                ", dia=" + dia +
                ", mes=" + mes +
                ", anio=" + anio +
                '}';
    }
}
