package ar.com.seguridadalerta.api.model;

import java.io.Serializable;
import java.util.Objects;

public class Novedad implements Serializable {

    public Novedad() {
    }

    public Novedad(long idSocio, long idObjetivo, String imagenUrl, String descripcion) {
        this.idSocio = idSocio;
        this.idObjetivo = idObjetivo;
        this.imagenUrl = imagenUrl;
        this.descripcion = descripcion;
    }

    private long idSocio;
    private long idObjetivo;
    private String imagenUrl;
    private String descripcion;

    public long getIdSocio() {
        return idSocio;
    }

    public void setIdSocio(long idSocio) {
        this.idSocio = idSocio;
    }

    public long getIdObjetivo() {
        return idObjetivo;
    }

    public void setIdObjetivo(long idObjetivo) {
        this.idObjetivo = idObjetivo;
    }

    public String getImagenUrl() {
        return imagenUrl;
    }

    public void setImagenUrl(String imagenUrl) {
        this.imagenUrl = imagenUrl;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Novedad)) return false;
        Novedad novedad = (Novedad) o;
        return imagenUrl.equals(novedad.imagenUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(imagenUrl);
    }

    @Override
    public String toString() {
        return "Novedad{" +
                "idSocio=" + idSocio +
                ", idObjetivo=" + idObjetivo +
                ", imagenUrl='" + imagenUrl + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
