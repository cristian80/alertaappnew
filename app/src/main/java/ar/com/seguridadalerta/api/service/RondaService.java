package ar.com.seguridadalerta.api.service;

import android.content.Context;

import java.util.List;

import ar.com.seguridadalerta.api.GsonHelper;
import ar.com.seguridadalerta.api.NetworkClient;
import ar.com.seguridadalerta.api.RondaApi;
import ar.com.seguridadalerta.api.model.QrGpsData;
import ar.com.seguridadalerta.api.model.RondaResult;
import ar.com.seguridadalerta.api.model.RondasResult;
import ar.com.seguridadalerta.sharedPref.PersistenceHelper;
import io.reactivex.Observable;
import retrofit2.Retrofit;

public class RondaService {

    private static RondaService novedadService;

    public static RondaService getInstance() {
        if(novedadService == null) {
            novedadService = new RondaService();
        }
        return novedadService;
    }

    public Observable<RondaResult> sendQrTrackata(QrGpsData qrGpsData, Context context) {
        Observable<RondaResult> rondaResult;
        Retrofit retrofit = NetworkClient.getRetrofitClient(context);
        RondaApi rondaApi = retrofit.create(RondaApi.class);
        rondaResult = rondaApi.sendQrTrackata(qrGpsData);
        PersistenceHelper persistenceHelper = new PersistenceHelper(context);
        persistenceHelper.saveData(qrGpsData.getCustomId(), GsonHelper.getGsonBuilder().toJson(qrGpsData));
        return rondaResult;
    }

    public Observable<RondasResult> resendQrTrackata(List<QrGpsData> qrGpsDataList, Context context) {
        Observable<RondasResult> rondasResult;
        Retrofit retrofit = NetworkClient.getRetrofitClient(context);
        RondaApi rondaApi = retrofit.create(RondaApi.class);
        rondasResult = rondaApi.resendQrTrackata(qrGpsDataList);
        return rondasResult;
    }
}
