package ar.com.seguridadalerta.api.service;

import android.content.Context;

import ar.com.seguridadalerta.api.LogoutApi;
import ar.com.seguridadalerta.api.NetworkClient;
import ar.com.seguridadalerta.api.model.GenericApiResult;
import ar.com.seguridadalerta.api.model.Logout;
import io.reactivex.Observable;
import retrofit2.Retrofit;

public class LogoutService {

    private static LogoutService logoutService;

    public static LogoutService getInstance() {
        if(logoutService == null) {
            logoutService = new LogoutService();
        }
        return logoutService;
    }

    public Observable<GenericApiResult> logout(Logout logout, Context context) {

        Observable<GenericApiResult> logoutResult;
        Retrofit retrofit = NetworkClient.getRetrofitClient(context);
        LogoutApi logoutApi = retrofit.create(LogoutApi.class);
        logoutResult = logoutApi.logout(logout);
        return logoutResult;
    }
}
