package ar.com.seguridadalerta.api.model;

import java.io.Serializable;
import java.util.Objects;

public class Socio implements Serializable {

    public Socio() {

    }

    public Socio(long idSocio, String legajo, String nombre, String apellido) {
        this.idSocio = idSocio;
        this.legajo = legajo;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    private long idSocio;
    private String legajo;
    private String nombre;
    private String apellido;

    public long getIdSocio() {
        return idSocio;
    }

    public void setIdSocio(long idSocio) {
        this.idSocio = idSocio;
    }

    public String getLegajo() {
        return legajo;
    }

    public void setLegajo(String legajo) {
        this.legajo = legajo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Socio)) return false;
        Socio socio = (Socio) o;
        return idSocio == socio.idSocio;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idSocio);
    }
}
