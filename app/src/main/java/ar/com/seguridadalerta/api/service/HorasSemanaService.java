package ar.com.seguridadalerta.api.service;

import android.content.Context;

import ar.com.seguridadalerta.api.HorasSemanaApi;
import ar.com.seguridadalerta.api.NetworkClient;
import ar.com.seguridadalerta.api.model.HorasSemanaQuery;
import ar.com.seguridadalerta.api.model.HorasSemanaResult;
import io.reactivex.Observable;
import retrofit2.Retrofit;

public class HorasSemanaService {

    private static HorasSemanaService horasSemanaService;

    public static HorasSemanaService getInstance() {
        if(horasSemanaService == null) {
            horasSemanaService = new HorasSemanaService();
        }
        return horasSemanaService;
    }

    public Observable<HorasSemanaResult> getHoras(HorasSemanaQuery horasSemanaQuery, Context context) {
        Observable<HorasSemanaResult> horasSemanaResultObservable;
        Retrofit retrofit = NetworkClient.getRetrofitClient(context);
        HorasSemanaApi horasSemanaApi = retrofit.create(HorasSemanaApi.class);
        horasSemanaResultObservable = horasSemanaApi.getHoras(horasSemanaQuery);
        return horasSemanaResultObservable;
    }
}
