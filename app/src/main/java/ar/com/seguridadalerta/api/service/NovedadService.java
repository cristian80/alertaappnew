package ar.com.seguridadalerta.api.service;

import android.content.Context;

import ar.com.seguridadalerta.api.NetworkClient;
import ar.com.seguridadalerta.api.NovedadApi;
import ar.com.seguridadalerta.api.model.GenericApiResult;
import ar.com.seguridadalerta.api.model.Novedad;
import io.reactivex.Observable;
import retrofit2.Retrofit;

public class NovedadService {

    private static NovedadService novedadService;

    public static NovedadService getInstance() {
        if(novedadService == null) {
            novedadService = new NovedadService();
        }
        return novedadService;
    }

    public Observable<GenericApiResult> postNovedad(Novedad novedad, Context context) {
        Observable<GenericApiResult> novedadResult;
        Retrofit retrofit = NetworkClient.getRetrofitClient(context);
        NovedadApi novedadApi = retrofit.create(NovedadApi.class);
        novedadResult = novedadApi.postNovedad(novedad);
        return novedadResult;
    }
}
