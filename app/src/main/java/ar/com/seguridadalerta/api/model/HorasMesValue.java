package ar.com.seguridadalerta.api.model;

import java.io.Serializable;
import java.util.Objects;

public class HorasMesValue implements Serializable {

    public HorasMesValue() {
    }

    public HorasMesValue(int semana1, int semana2, int semana3, int semana4, int semana5, int mes, int total) {
        this.semana1 = semana1;
        this.semana2 = semana2;
        this.semana3 = semana3;
        this.semana4 = semana4;
        this.semana5 = semana5;
        this.mes = mes;
        this.total = total;
    }

    private float semana1;
    private float semana2;
    private float semana3;
    private float semana4;
    private float semana5;
    private float mes;
    private float total;

    public float getSemana1() {
        return semana1;
    }

    public void setSemana1(float semana1) {
        this.semana1 = semana1;
    }

    public float getSemana2() {
        return semana2;
    }

    public void setSemana2(float semana2) {
        this.semana2 = semana2;
    }

    public float getSemana3() {
        return semana3;
    }

    public void setSemana3(float semana3) {
        this.semana3 = semana3;
    }

    public float getSemana4() {
        return semana4;
    }

    public void setSemana4(float semana4) {
        this.semana4 = semana4;
    }

    public float getSemana5() {
        return semana5;
    }

    public void setSemana5(float semana5) {
        this.semana5 = semana5;
    }

    public float getMes() {
        return mes;
    }

    public void setMes(float mes) {
        this.mes = mes;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HorasMesValue)) return false;
        HorasMesValue that = (HorasMesValue) o;
        return semana1 == that.semana1 && semana2 == that.semana2 && semana3 == that.semana3 &&
                semana4 == that.semana4 && semana5 == that.semana5 && mes == that.mes &&
                total == that.total;
    }

    @Override
    public int hashCode() {
        return Objects.hash(semana1, semana2, semana3, semana4, semana5, mes, total);
    }

    @Override
    public String toString() {
        return "HorasMesValue{" +
                "semana1=" + semana1 + ", semana2=" + semana2 + ", semana3=" + semana3 +
                ", semana4=" + semana4 + ", semana5=" + semana5 + ", mes=" + mes +
                ", total=" + total + '}';
    }
}
