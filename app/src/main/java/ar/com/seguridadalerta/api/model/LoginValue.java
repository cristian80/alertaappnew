package ar.com.seguridadalerta.api.model;

import android.util.Log;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class LoginValue implements Serializable {

    private static final String TAG = "LoginValue";

    public LoginValue() {
    }

    public LoginValue(Socio socio, Objetivo objetivo) {
        this.socio = socio;
        this.objetivo = objetivo;
    }

    private long loginId;
    private long cronoId;
    private Socio socio;
    private Objetivo objetivo;
    private String token;
    private long desde;
    private long hasta;
    private float total;


    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    public Objetivo getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(Objetivo objetivo) {
        this.objetivo = objetivo;
    }

    public long getCronoId() {
        return cronoId;
    }

    public void setCronoId(long cronoId) {
        this.cronoId = cronoId;
    }

    public long getDesde() {
        return desde;
    }

    public void setDesde(long desde) {
        this.desde = desde;
    }

    public long getHasta() {
        return hasta;
    }

    public void setHasta(long hasta) {
        this.hasta = hasta;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getLoginId() {
        return loginId;
    }

    public void setLoginId(long loginId) {
        this.loginId = loginId;
    }

    public Date getHastaFecha() {

        return new Date(getHasta());
    }

    public Date getDesdeFecha() {

        return new Date(getDesde());
    }

    public boolean isTurnoCerrado() {
        Date fechaActual = new Date();
        Log.e( TAG, "Fecha actual: " + fechaActual.toString());
        Log.e( TAG, "Fecha cierre turno: " + getHastaFecha().toString());
        return fechaActual.after(getHastaFecha());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoginValue)) return false;
        LoginValue that = (LoginValue) o;
        return socio.equals(that.socio) &&
                objetivo.equals(that.objetivo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(socio, objetivo);
    }
}
