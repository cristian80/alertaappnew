package ar.com.seguridadalerta.api;

import ar.com.seguridadalerta.api.model.GenericApiResult;
import ar.com.seguridadalerta.api.model.Logout;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LogoutApi {
    @POST("logout/")
    Observable<GenericApiResult> logout(@Body Logout logout);
}
