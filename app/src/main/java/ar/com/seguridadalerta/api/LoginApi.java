package ar.com.seguridadalerta.api;

import ar.com.seguridadalerta.api.model.LoginResult;
import ar.com.seguridadalerta.api.model.LoginUser;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginApi {

    @POST("login")
    Observable<LoginResult> login(@Body LoginUser user);
}