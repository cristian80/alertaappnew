package ar.com.seguridadalerta.api.model;

import java.util.Objects;

public class RondaResult implements ApiResult {
    public RondaResult() {
    }

    public RondaResult(String token, QrGpsData value, String message, String timestamp, String status) {
        this.token = token;
        this.value = value;
        this.message = message;
        this.timestamp = timestamp;
        this.status = status;
    }

    private String token;
    private QrGpsData value;
    private String message;
    private String timestamp;
    private String status;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public QrGpsData getValue() {
        return value;
    }

    public void setValue(QrGpsData value) {

        this.value = value;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getTimestamp() {
        return timestamp;
    }

    @Override
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getAmz_key() {
        return null;
    }

    @Override
    public void setAmz_key(String amz_key) {

    }

    @Override
    public String getAmz_secret() {
        return null;
    }

    @Override
    public void setAmz_secret(String amz_secret) {

    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoginResult)) return false;
        LoginResult that = (LoginResult) o;
        return token.equals(that.getToken());
    }

    @Override
    public int hashCode() {
        return Objects.hash(token);
    }
}
