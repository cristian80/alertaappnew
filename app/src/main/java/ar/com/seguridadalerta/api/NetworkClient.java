package ar.com.seguridadalerta.api;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import ar.com.seguridadalerta.BuildConfig;
import ar.com.seguridadalerta.LoginActivity;
import ar.com.seguridadalerta.api.model.LoginResult;
import ar.com.seguridadalerta.api.model.LoginValue;
import ar.com.seguridadalerta.api.service.UserService;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkClient {
    private static Retrofit retrofit;
    private static String TAG = "NetworkClient";
    /*
    This public static method will return Retrofit client
    anywhere in the appplication
    */
    public static Retrofit getRetrofitClient(Context context) {
        //If condition to ensure we don't create multiple retrofit instances in a single application
        if (retrofit == null) {
            //Defining the Retrofit using Builder
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .addInterceptor(chain -> {
                        Request request = chain.request();
                        okhttp3.Response response = chain.proceed(request);
                        // if 403 error token is invalid user must logout
                        if (response.code() == 403) {
                            LoginValue currentUser = UserService.getInstance(context).getUser().getValue();
                            Answers.getInstance().logCustom(new CustomEvent("Token Expirado")
                                    .putCustomAttribute("Fecha", new Date().toString())
                                    .putCustomAttribute("Usuario", String.format("%s %s",
                                            currentUser.getSocio().getNombre(),
                                            currentUser.getSocio().getApellido())));
                            logoutUser(context);
                            return response;
                        }
                        return response;
                    })
                    .addInterceptor(chain -> {
                        Request request = chain.request();
                        LoginResult user = UserService.getInstance(context).getUser();
                        if(user != null) {
                            String token = UserService.getInstance(context).getUser().getToken();
                            if(token != null && token.length() > 0) {
                                request = chain.request().newBuilder().addHeader("Authorization", token).build();
                                Log.d("======= TOKEN ======", token);
                            }
                        }
                        return chain.proceed(request);
                    })
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();

            String BASE_URL = "https://api-alerta.herokuapp.com/";
            if (BuildConfig.DEBUG) {
                Log.d(TAG, ">>>>>>>>>>>>> URL SERVER TEST <<<<<<<<<<<<<<<<<<");
                BASE_URL = "https://api-alerta-test.herokuapp.com/";
            }
            else {
                Log.d(TAG, ">>>>>>>>>>>>> URL SERVER PRODUCCION <<<<<<<<<<<<<<<<<<");
            }
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonHelper.getGsonBuilder()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }

    private static void logoutUser(Context context) {
        UserService.getInstance(context).setUser(null);
        Intent loginIntent = new Intent(context, LoginActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(loginIntent);
    }
}
