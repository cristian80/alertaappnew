package ar.com.seguridadalerta.api;

import ar.com.seguridadalerta.api.model.GenericApiResult;
import ar.com.seguridadalerta.api.model.Novedad;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface NovedadApi {
    @POST("novedad/")
    Observable<GenericApiResult> postNovedad(@Body Novedad novedad);
}
