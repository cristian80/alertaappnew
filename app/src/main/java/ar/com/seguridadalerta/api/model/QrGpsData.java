package ar.com.seguridadalerta.api.model;

import java.util.Date;

import ar.com.seguridadalerta.api.util.QRTrackDataUtil;

public class QrGpsData {
    private Integer id;
    private Date fecha;
    private String lat;
    private String lon;
    private String data;
    private String nombre;



    public QrGpsData() {
    }

    public QrGpsData(Date fecha, String lat, String lon, String data) {
        this.fecha = fecha;
        this.lat = lat;
        this.lon = lon;
        this.data = data;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCustomId(){
        return QRTrackDataUtil.generateInternalId(this);
    }
}
