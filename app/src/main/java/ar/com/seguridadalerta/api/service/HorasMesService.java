package ar.com.seguridadalerta.api.service;

import android.content.Context;

import ar.com.seguridadalerta.api.HorasMesApi;
import ar.com.seguridadalerta.api.NetworkClient;
import ar.com.seguridadalerta.api.model.HorasMesQuery;
import ar.com.seguridadalerta.api.model.HorasMesResult;
import io.reactivex.Observable;
import retrofit2.Retrofit;

public class HorasMesService {

    private static HorasMesService horasMesService;

    public static HorasMesService getInstance() {
        if(horasMesService == null) {
            horasMesService = new HorasMesService();
        }
        return horasMesService;
    }

    public Observable<HorasMesResult> getHoras(HorasMesQuery horasMesQuery, Context context) {
        Observable<HorasMesResult> horasResult;
        Retrofit retrofit = NetworkClient.getRetrofitClient(context);
        HorasMesApi horasMesApi = retrofit.create(HorasMesApi.class);
        horasResult = horasMesApi.getHoras(horasMesQuery);
        return horasResult;
    }
}
