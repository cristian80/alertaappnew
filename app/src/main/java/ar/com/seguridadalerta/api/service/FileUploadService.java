package ar.com.seguridadalerta.api.service;

import android.content.Context;
import android.util.Log;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.UserStateDetails;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;

import ar.com.seguridadalerta.BuildConfig;

public class FileUploadService {

    private static String TAG = "FileUploadService";
    private static  FileUploadService fileUploadService;

    private FileUploadService(Context context) {
        // Initialize the AWSMobileClient if not initialized
        AWSMobileClient.getInstance().initialize(context.getApplicationContext(), new Callback<UserStateDetails>() {
            @Override
            public void onResult(UserStateDetails userStateDetails) {
                Log.i(TAG, "AWSMobileClient initialized. User State is " + userStateDetails.getUserState());
            }

            @Override
            public void onError(Exception e) {
                Log.e(TAG, "Initialization error.", e);
            }
        });
    }

    public static FileUploadService getInstance(Context context) {
        if(fileUploadService == null) {
            fileUploadService = new FileUploadService(context);
        }
        return fileUploadService;
    }

    //images/login/
    //images/novedades/
    public void uploadFile(String path, String fileName, File file, Context context, TransferListener listener, String secret, String key) {
        BasicAWSCredentials credentials = new BasicAWSCredentials(key, secret);
        AmazonS3Client s3Client = new AmazonS3Client(credentials);

        TransferUtility transferUtility =
                TransferUtility.builder()
                        .context(context.getApplicationContext())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(s3Client)
                        .build();

        String BUCKET = "alerta-prod";
        if (BuildConfig.DEBUG) {
            Log.d(TAG, ">>>>>>>>>>>>> BUCKET TEST <<<<<<<<<<<<<<<<<<");
            BUCKET = "alerta-test";
        }
        else {
            Log.d(TAG, ">>>>>>>>>>>>> BUCKET PRODUCCION <<<<<<<<<<<<<<<<<<");
        }

        TransferObserver uploadObserver =
                transferUtility.upload(BUCKET,path + fileName, file);
        uploadObserver.setTransferListener(listener);
    }

}
