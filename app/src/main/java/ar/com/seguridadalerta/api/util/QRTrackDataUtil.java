package ar.com.seguridadalerta.api.util;

import java.text.SimpleDateFormat;
import java.util.Locale;

import ar.com.seguridadalerta.api.model.QrGpsData;

public class QRTrackDataUtil {
    private static SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy-hhmm", Locale.getDefault());

    public static String generateInternalId(QrGpsData data){
        return sdf.format(data.getFecha())+"-"+data.getData();
    }
}
