package ar.com.seguridadalerta.api.model;

import java.io.Serializable;
import java.util.Objects;

public class HorasSemanaValue implements Serializable {

    public HorasSemanaValue() {
    }

    public HorasSemanaValue(String dia, float horas) {
        this.dia = dia;
        this.horas = horas;
    }

    private String dia;
    private float horas;

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public float getHoras() {
        return horas;
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HorasSemanaValue)) return false;
        HorasSemanaValue that = (HorasSemanaValue) o;
        return Float.compare(that.horas, horas) == 0 &&
                Objects.equals(dia, that.dia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dia, horas);
    }

    @Override
    public String toString() {
        return "HorasSemanaValue{" +
                "dia='" + dia + '\'' +
                ", horas=" + horas +
                '}';
    }
}
