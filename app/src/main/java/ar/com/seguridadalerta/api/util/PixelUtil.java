package ar.com.seguridadalerta.api.util;

import android.content.Context;

public class PixelUtil {
    public static int dpToPixels(int dp, Context context) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dp * scale + 0.5f);
    }
}
