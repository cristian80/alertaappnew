package ar.com.seguridadalerta.api;

import ar.com.seguridadalerta.api.model.HorasSemanaQuery;
import ar.com.seguridadalerta.api.model.HorasSemanaResult;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface HorasSemanaApi {
    @POST("socio/horas/semana")
    Observable<HorasSemanaResult> getHoras(@Body HorasSemanaQuery horasSemanaQuery);
}
