package ar.com.seguridadalerta.api;

import java.util.List;

import ar.com.seguridadalerta.api.model.QrGpsData;
import ar.com.seguridadalerta.api.model.RondaResult;
import ar.com.seguridadalerta.api.model.RondasResult;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RondaApi {
    @POST("/qrgps/ronda")
    Observable<RondaResult> sendQrTrackata(@Body QrGpsData qrGpsData);

    @POST("/qrgps/rondas")
    Observable<RondasResult> resendQrTrackata(@Body List<QrGpsData> qrGpsDataList);
}
