package ar.com.seguridadalerta.api;

import ar.com.seguridadalerta.api.model.HorasMesQuery;
import ar.com.seguridadalerta.api.model.HorasMesResult;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface HorasMesApi {
    @POST("socio/horas")
    Observable<HorasMesResult> getHoras(@Body HorasMesQuery horasMesQuery);
}
