package ar.com.seguridadalerta;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Date;

import ar.com.seguridadalerta.api.model.LoginValue;
import ar.com.seguridadalerta.api.model.QrGpsData;
import ar.com.seguridadalerta.api.model.RondaResult;
import ar.com.seguridadalerta.api.model.RondasResult;
import ar.com.seguridadalerta.api.service.RondaService;
import ar.com.seguridadalerta.api.service.UserService;
import ar.com.seguridadalerta.sharedPref.PersistenceHelper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.observers.BlockingBaseObserver;
import io.reactivex.schedulers.Schedulers;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.INTERNET;

public class QRActivity extends BaseMenuActivity {

    private static final String TAG = "QRActivity";
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private PersistenceHelper persistenceHelper;

    private final static int ALL_PERMISSIONS_RESULT = 101;
    private Button btn_scan;
    private Button btn_reSend;
    private TextView txt_msg;
    private LocationTrack locationTrack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_qr);
        super.onCreate(savedInstanceState);

        persistenceHelper = new PersistenceHelper(this);
        checkForPermissions();
        btn_scan = findViewById(R.id.scan);
        btn_reSend = findViewById(R.id.reintentar);
        txt_msg = findViewById(R.id.msg);


        btn_scan.setOnClickListener(view -> {
            btn_scan.setEnabled(false);
            initScanner();
        });

        btn_reSend.setOnClickListener(view -> {
            btn_reSend.setEnabled(false);
            resendData();
        });

        checkDataSended();
        locationTrack = new LocationTrack();
    }




    private void initScanner() {
            IntentIntegrator integrator = new IntentIntegrator(this);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
            integrator.setPrompt(getString(R.string.escaneando));
            integrator.setCameraId(0);
            integrator.setBeepEnabled(true);
            integrator.setBarcodeImageEnabled(false);
            integrator.setOrientationLocked(false);
            integrator.setCaptureActivity(com.journeyapps.barcodescanner.CaptureActivity.class);
            integrator.initiateScan();
        btn_scan.setEnabled(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        locationTrack.stopListener(QRActivity.this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        locationTrack.stopListener(QRActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        int CAM_SCAN_ACTION = 49374;
        if (requestCode == CAM_SCAN_ACTION) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null) {
                if (result.getContents() == null) {
                    Toast.makeText(this, QRActivity.this.getString(R.string.cancelaste_qr), Toast.LENGTH_LONG).show();
                } else {
                    String[] location = locationTrack.getGpsLocation(QRActivity.this);
                    if(location != null) {

                        final QrGpsData qrGpsData = new QrGpsData();
                        String contents = result.getContents();
                        if (contents.length() > 100) {
                            contents = contents.substring(0, 100);
                        }
                        qrGpsData.setData(contents);
                        qrGpsData.setFecha(new Date());
                        qrGpsData.setLat(String.format("%s", location[0]));
                        qrGpsData.setLon(String.format("%s", location[1]));
                        LoginValue currentUser = UserService.getInstance(QRActivity.this).getUser().getValue();
                        qrGpsData.setNombre(currentUser.getSocio().getApellido() + ", " + currentUser.getSocio().getNombre());

                        RondaService.getInstance().sendQrTrackata(qrGpsData, QRActivity.this)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.newThread())
                                .subscribe(new BlockingBaseObserver<RondaResult>() {
                                    @Override
                                    public void onNext(RondaResult genericApiResult) {
                                        showToast(QRActivity.this.getString(R.string.ronda_envio_correcto));
                                        persistenceHelper.removeData(qrGpsData.getCustomId());
                                        checkDataSended();
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        showToast(getString(R.string.error_ronda));
                                        checkDataSended();
                                    }
                                });
                    } else {
                        showToast(getString(R.string.error_coordenadas));
                    }

                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void checkForPermissions() {
        permissions.add(INTERNET);
        permissions.add(CAMERA);
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.toArray(new String[0]), ALL_PERMISSIONS_RESULT);
            }
        }
    }


    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<>();

        for (String perm : wanted) {
            if (hasNotPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasNotPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED);
        }
        return false;
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {

        if (requestCode == ALL_PERMISSIONS_RESULT) {
            for (String perms : permissionsToRequest) {
                if (hasNotPermission(perms)) {
                    permissionsRejected.add(perms);
                }
            }

            if (permissionsRejected.size() > 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                        showMessageOKCancel("Estos permisos son obligatorios para acceder a la aplicacion. Habilitelos por favor.",
                                (dialog, which) -> requestPermissions(permissionsRejected.toArray(new String[0]), ALL_PERMISSIONS_RESULT),
                                (dialog, which) -> requestPermissions(permissionsRejected.toArray(new String[0]), ALL_PERMISSIONS_RESULT)
                        );
                    }
                }

            }
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener cancelListener) {
        new AlertDialog.Builder(QRActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", cancelListener)
                .create()
                .show();
    }

    private void showToast(final String message){
        runOnUiThread(() -> Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show());
    }

    private boolean allDataSended(){
        return persistenceHelper.getAll()==null;
    }

    private void showResend(final boolean show){
        runOnUiThread(() -> {
            if(show){
                txt_msg.setVisibility(View.VISIBLE);
                btn_reSend.setVisibility(View.VISIBLE);
            }else{
                txt_msg.setVisibility(View.INVISIBLE);
                btn_reSend.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void checkDataSended() {
        showResend(!allDataSended());
        persistenceHelper.showContent();
    }


    private void resendData() {
        RondaService.getInstance().resendQrTrackata(persistenceHelper.getAll(), QRActivity.this)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new BlockingBaseObserver<RondasResult>() {
                    @Override
                    public void onNext(RondasResult rondasResult) {
                        showToast(getString(R.string.rondas_envio_correcto));
                        persistenceHelper.removeAllData();
                        checkDataSended();
                        runOnUiThread(() -> btn_reSend.setEnabled(true));
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        showToast(getString(R.string.error_ronda_resend));
                        checkDataSended();
                        runOnUiThread(() -> btn_reSend.setEnabled(true));
                    }
                });
    }
}
