package ar.com.seguridadalerta;

import android.app.Application;
import android.content.Intent;
import android.os.Build;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        //Intent intent = new Intent(this, TransferService.class);
        //getApplicationContext().startService(intent);
        Intent intent = new Intent(getApplicationContext(), TurnoAndroidService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getApplicationContext().startForegroundService(intent);
        } else {
            getApplicationContext().startService(intent);
        }
    }
}
